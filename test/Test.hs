module Main where

import Network.Transport.Tests
import Control.Concurrent.MVar
import Control.Monad
import Control.Monad.IO.Class
import Data.ByteString (ByteString)
import Data.Map (Map)
import qualified Data.Map as Map
import Network.Transport.Tests.Auxiliary
import Network.Transport.Util
import Network.Transport.CADET
import Network.Transport

main :: IO ()
main = do
  Right transport <- newTransport
  runTests
    [ ("PingPong",              testPingPong transport numPings)
    , ("EndPoints",             testEndPoints transport numPings)
    , ("Connections",           testConnections transport numPings)
    -- , ("CloseOneConnection",    testCloseOneConnection transport numPings)
    -- , ("CloseOneDirection",     testCloseOneDirection transport numPings)
    -- , ("CloseReopen",           testCloseReopen transport numPings)
    -- , ("ParallelConnects",      testParallelConnects transport numPings)
    -- , ("SendAfterClose",        testSendAfterClose transport 100)
    , ("Crossing",              testCrossing transport 5)
    -- , ("CloseTwice",            testCloseTwice transport 100)
    -- , ("ConnectToSelf",         testConnectToSelf transport numPings)
    -- , ("ConnectToSelfTwice",    testConnectToSelfTwice transport numPings)
    -- , ("CloseSelf",             testCloseSelf newTransport)
    -- , ("CloseEndPoint",         testCloseEndPoint transport numPings)
    -- XXX: require transport communication
    -- ("CloseTransport",        testCloseTransport newTransport)
    -- , ("ConnectClosedEndPoint", testConnectClosedEndPoint transport)
    -- , ("ExceptionOnReceive",    testExceptionOnReceive newTransport)
    -- , ("SendException",         testSendException newTransport)
    -- , ("Kill",                  testKill (return killTransport) 1000)
    ]
  closeTransport transport
  where
    newTransport = (either (Left . show) (Right) <$> createTransport (CADETParameters { configPath = "../gnunet1.conf" }))
    numPings = 1 :: Int
