module Network.Transport.CADET where

import Data.Atomics.Counter (AtomicCounter, newCounter, incrCounter)
import Control.Concurrent (forkOS)
import Control.Concurrent.MVar
import Control.Concurrent.STM.TChan
import Control.Monad
import Control.Monad.STM
import Control.Exception (IOException)
import qualified STMContainers.Map as STM.Map
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as C
import Data.Bits
import Data.Semigroup ((<>))
import Data.String (fromString)
import Foreign.Ptr
import Foreign.C.Types
import Foreign.Storable
import Foreign.Marshal.Alloc
import Foreign.Marshal.Utils
import Network.Transport
import Network.GNUnet.Common
import Network.GNUnet.Configuration
import Network.GNUnet.Crypto
import Network.GNUnet.Scheduler
import Network.GNUnet.CADET
import qualified Network.GNUnet.CADET.Types as CADET
import qualified Network.GNUnet.MQ as MQ
import Bindings.GNUnet.GnunetCommon
import Bindings.GNUnet.GnunetConfigurationLib
import Bindings.GNUnet.GnunetCryptoTypes
import Bindings.GNUnet.GnunetHelloLib
import Bindings.GNUnet.GnunetSchedulerLib
import Bindings.GNUnet.GnunetTransportHelloService
import Bindings.GNUnet.GnunetMqLib
import Bindings.GNUnet.GnunetCadetService
import Bindings.GNUnet.GnunetProtocols

{-
data TransportState
    = TransportValid !ValidTransportState
    | TransportClosed
-}

data CADETParameters = CADETParameters {
    configPath :: FilePath
}

data CADETTransport = CADETTransport {
    _pid :: !(Maybe PeerIdentity)
  , _state :: !(MVar ValidTransportState)
  , _gnunetConfig :: Ptr C'GNUNET_CONFIGURATION_Handle
}

data ValidTransportState = ValidTransportState {
    _cadet :: Ptr C'GNUNET_CADET_Handle
  , _tasks :: TChan (IO ())
  , _lastEndpointId :: AtomicCounter
}

type CCN = Int
type EPMap = STM.Map.Map CCN (C.ByteString, Maybe C.ByteString) -- Nothing = we don't know yet

data CADETEndPoint = CADETEndPoint {
    _port :: Ptr C'GNUNET_CADET_Port
  , _tchan :: TChan Event
  , _connmap :: EPMap
}

createTransport :: CADETParameters -> IO (Either IOException Transport)
createTransport params = do
    logSetup "cadet-test" "INFO"

    cfg <- load $ configPath params

    tasks <- newTChanIO

    state <- newEmptyMVar

    forkOS . run $ do
        logNocheck c'GNUNET_ERROR_TYPE_DEBUG "Connecting to CADET service\n"
        mh <- c'GNUNET_CADET_connect cfg
        -- FIXME: needs better short-circuiting
        when (nullPtr == mh) $ c'GNUNET_SCHEDULER_shutdown >> undefined
        addShutdown $ do
            c'GNUNET_CADET_disconnect mh

        lastEndpointId <- newCounter 0

        putMVar state $ ValidTransportState mh tasks lastEndpointId

        -- Run incoming tasks
        -- FIXME: polling here in lieu of better GS / RTS integration
        let checkTasks = void . flip (c'GNUNET_SCHEDULER_add_delayed_with_priority 1000000 c'GNUNET_SCHEDULER_PRIORITY_BACKGROUND) nullPtr =<< (mk'GNUNET_SCHEDULER_TaskCallback . const $ do
                a' <- atomically . tryReadTChan $ tasks
                case a' of
                    Nothing -> return ()
                    Just a ->
                        void . flip c'GNUNET_SCHEDULER_add_now nullPtr =<< (mk'GNUNET_SCHEDULER_TaskCallback . const $ a)
                checkTasks
                )
        checkTasks

    return . pure $ Transport {
        newEndPoint = newEndPoint_ $ CADETTransport Nothing state cfg
      , closeTransport = c'GNUNET_SCHEDULER_shutdown
    }

newEndPoint_ :: CADETTransport -> IO (Either (TransportError NewEndPointErrorCode) EndPoint)
newEndPoint_ ts = do
    state <- readMVar (_state ts)

    pid <- getPId ts

    nextEndpointId <- incrCounter 1 (_lastEndpointId state)

    chan <- newTChanIO

    connmap <- STM.Map.newIO

    mq_msg_h <- MQ.mkMessageHandler validate (handle connmap chan) C'GNUNET_MESSAGE_TYPE_CADET_CLI

    port <- with mq_msg_h $ openPort (_cadet state) (hash $ C.pack . show $ nextEndpointId) (channelIncoming connmap chan) Nothing (incomingChannelEnded connmap chan)


    return . pure $ EndPoint {
        receive = atomically . readTChan $ chan
      , address = EndPointAddress $ (i2s_full' pid) <> ":" <> (C.pack . show $ nextEndpointId)
      , connect = connect_ state nextEndpointId chan
      , newMulticastGroup = return $ Left $ TransportError NewMulticastGroupUnsupported "Multicast not supported"
      , resolveMulticastGroup = return . return . Left $ TransportError ResolveMulticastGroupUnsupported "Multicast not supported"
      , closeEndPoint = c'GNUNET_CADET_close_port port -- TODO: ClosedEndPointState
    }

connect_ :: ValidTransportState -> CCN -> TChan Event -> EndPointAddress -> Reliability -> ConnectHints -> IO (Either (TransportError ConnectErrorCode) Connection)
connect_ st localShortEpId chan addr _ _ = do
    let [pid_, port_] = B.split 58 . endPointAddressToByteString $ addr -- by ':'
    let pid = PeerIdentity . C'GNUNET_PeerIdentity . getCEddsaPublicKey . fromString @EddsaPublicKey . C.unpack $ pid_ -- FIXME
    let port = hash port_
    logNocheck c'GNUNET_ERROR_TYPE_DEBUG $ "Connecting to " ++ show (pid, port) ++ "\n"
    let opts = c'GNUNET_CADET_OPTION_DEFAULT .|. c'GNUNET_CADET_OPTION_RELIABLE

    state <- newEmptyMVar

    atomically . writeTChan (_tasks st) $ do
        mq_msg_h <- MQ.mkMessageHandler validate nullHandler C'GNUNET_MESSAGE_TYPE_CADET_CLI

        ch <- with mq_msg_h $ createChannel (_cadet st) pid port opts Nothing outcomingChannelEnded
        mq <- c'GNUNET_CADET_get_mq ch

        putMVar state (ch, mq)

    (ch, mq) <- readMVar state

    -- First msg is "negotiation": we send local epId so that remote knows not just
    -- [Peer -> [Peer:Port]], but "source [endpoint] port" too.
    -- FIXME: proper serialization
    (MQ.msg C'GNUNET_MESSAGE_TYPE_CADET_CLI >=> c'GNUNET_MQ_send mq) $ C.pack . show $ localShortEpId

    return . pure $ Connection {
        send = \m -> do
            mapM_ (MQ.msg C'GNUNET_MESSAGE_TYPE_CADET_CLI >=> c'GNUNET_MQ_send mq) m
            return $ pure ()
      , close = do
            c'GNUNET_CADET_channel_destroy ch
    }

validate :: Ptr C'GNUNET_CADET_Channel -> C'GNUNET_MessageHeader -> IO CInt
-- Replicates gnunet-cadet.c
validate _ _ = return C'GNUNET_OK

nullHandler :: Ptr C'GNUNET_CADET_Channel -> C'GNUNET_MessageHeader -> B.ByteString -> IO ()
nullHandler _ _ _ = return ()

handle :: EPMap -> TChan Event -> Ptr C'GNUNET_CADET_Channel -> C'GNUNET_MessageHeader -> B.ByteString -> IO ()
handle epmap chan ch msg msg_text = do
    C.putStr msg_text

    ccn <- fromIntegral . c'GNUNET_CADET_ClientChannelNumber'channel_of_client . c'GNUNET_CADET_ChannelInfo'ccn <$> (peek =<< c'GNUNET_CADET_channel_get_info ch c'GNUNET_CADET_OPTION_CCN)

    {-
    if port == Nothing then
        if msg == negotiation then
            port := negotiation.port
        else
            close channel
    -}
    atomically $ do
        -- TODO: use focus API
        Just (initiator, port) <- STM.Map.lookup ccn epmap
        case port of
            Nothing -> STM.Map.delete ccn epmap >> STM.Map.insert (initiator, Just msg_text) ccn epmap >> writeTChan chan (ConnectionOpened (fromIntegral ccn) ReliableOrdered (EndPointAddress $ initiator <> ":" <> msg_text))
            Just _ -> writeTChan chan $ Received (fromIntegral ccn) [msg_text]

channelIncoming :: EPMap -> TChan Event -> Ptr C'GNUNET_CADET_Channel -> PeerIdentity -> IO (Ptr ())
channelIncoming epmap evchan channel initiator = do
    ccn <- fromIntegral . c'GNUNET_CADET_ClientChannelNumber'channel_of_client . c'GNUNET_CADET_ChannelInfo'ccn <$> (peek =<< c'GNUNET_CADET_channel_get_info channel c'GNUNET_CADET_OPTION_CCN)

    -- TODO: Note we don't close_port, which results in assertion at cadet_api.c:1207
    logNocheck c'GNUNET_ERROR_TYPE_DEBUG $ "Incoming connection from " ++ show initiator ++ " (ccn " ++ show ccn ++ ")" ++ "\n"

    atomically $ STM.Map.insert (i2s_full' initiator, Nothing) ccn epmap

    -- TODO: would be nicer to be IO (Ptr C'GNUNET_CADET_Channel)
    with (CADET.CADETState channel) $ return . castPtr

incomingChannelEnded :: EPMap -> TChan Event -> Ptr C'GNUNET_CADET_Channel -> IO ()
incomingChannelEnded epmap evchan channel = do
    ccn <- fromIntegral . c'GNUNET_CADET_ClientChannelNumber'channel_of_client . c'GNUNET_CADET_ChannelInfo'ccn <$> (peek =<< c'GNUNET_CADET_channel_get_info channel c'GNUNET_CADET_OPTION_CCN)

    atomically $ do
      STM.Map.delete ccn epmap
      writeTChan evchan $ ConnectionClosed (fromIntegral ccn)
    logNocheck c'GNUNET_ERROR_TYPE_DEBUG "Channel ended!\n"

outcomingChannelEnded :: Ptr C'GNUNET_CADET_Channel -> IO ()
outcomingChannelEnded channel = do
    logNocheck c'GNUNET_ERROR_TYPE_DEBUG "Channel ended!\n"

getPId :: CADETTransport -> IO PeerIdentity
getPId ts = do
    state <- readMVar (_state ts)

    our_pid <- newEmptyMVar
    atomically . writeTChan (_tasks state) $ do
        hello_handle <- newEmptyMVar
        putMVar hello_handle =<< flip (c'GNUNET_TRANSPORT_hello_get (_gnunetConfig ts) c'GNUNET_TRANSPORT_AC_ANY) nullPtr =<< (
            mk'GNUNET_TRANSPORT_HelloUpdateCallback $ \_ our_hello_header -> do
                when (nullPtr == our_hello_header) $ c'GNUNET_SCHEDULER_shutdown >> undefined
                our_hello <- castPtr <$> c'GNUNET_copy_message our_hello_header
                c'GNUNET_TRANSPORT_hello_get_cancel =<< readMVar hello_handle
                alloca $ \our_pid' -> do
                    assert' =<< (== C'GNUNET_OK) <$> c'GNUNET_HELLO_get_id our_hello our_pid'
                    putMVar our_pid =<< PeerIdentity <$> peek our_pid'
            )
    readMVar our_pid
