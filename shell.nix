{ nixpkgs ? import (fetchTarball "https://nixos.org/channels/nixos-18.03-small/nixexprs.tar.xz") { } }:
(import ./default.nix { inherit nixpkgs; }).env
