import Control.Monad
import Control.Concurrent
import qualified Data.ByteString.Char8 as C
import Network.Transport
import Network.Transport.CADET

main :: IO ()
main = do
    t <- createTransport $ CADETParameters { configPath = "gnunet1.conf" }
    e <- (\(Right x) -> x) <$> newEndPoint t
    c <- (\(Right x) -> x) <$> connect e (EndPointAddress "860X7QR3Y9GA4684DX65QMHTZJDV6ZRK01BN6XWATXEDP3KP3XWG:q") undefined undefined 
    let m = map (C.pack . show) [1..100]
    void $ send c m
    c1 <- (\(Right x) -> x) <$> connect e (EndPointAddress "860X7QR3Y9GA4684DX65QMHTZJDV6ZRK01BN6XWATXEDP3KP3XWG:w") undefined undefined 
    void $ send c1 m
    forM_ [1..3] . const $ print =<< receive e
    close c
    void $ send c1 m
    threadDelay 2000000
    close c1
    closeTransport t
