{ mkDerivation, atomic-primops, base, bindings-gnunet, bytestring
, containers, gnunet, network-transport, network-transport-tests
, stdenv, stm, stm-containers
}:
mkDerivation {
  pname = "network-transport-cadet";
  version = "0.0.0.1";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    atomic-primops base bindings-gnunet bytestring gnunet
    network-transport stm stm-containers
  ];
  executableHaskellDepends = [ base bytestring network-transport ];
  testHaskellDepends = [
    base bytestring containers network-transport
    network-transport-tests
  ];
  license = stdenv.lib.licenses.agpl3;
}
